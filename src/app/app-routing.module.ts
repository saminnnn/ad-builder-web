import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdComponent } from "./ad/ad.component";
import { ConfigureAdComponent } from "./configure-ad/configure-ad.component";

const routes: Routes = [
    {path: '', component: ConfigureAdComponent},
    {path: 'ad', component: AdComponent}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)], exports: [RouterModule]
})
export class AppRoutingModule{}