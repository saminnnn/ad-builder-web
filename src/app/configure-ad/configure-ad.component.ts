import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AnimationDirection, Variables } from '../variables';

@Component({
  selector: 'app-configure-ad',
  templateUrl: './configure-ad.component.html',
  styleUrls: ['./configure-ad.component.css']
})
export class ConfigureAdComponent implements OnInit {
  variable: Variables;
  activeTab = 1;
  frameCount = 1;
  frameList: number[] = [1]; 
  frameCountAlert: string;
  change: boolean = false;
  adDimension = {width: '300px', height: '250px'};
  link = environment.baseUrl + '/variables'

  constructor(private http: HttpClient) { 
    this.variable = new Variables();
    this.frameCountChange();
  }

  ngOnInit(): void { this.replay() }

  replay() {
    this.adDimension = {width: this.variable.width + 'px', height: this.variable.height + 'px'};
    this.change = true;
    
    setTimeout(() => this.change = false, 500);
  }

  chanegActive(tabIndex: number) { this.activeTab = tabIndex; }
  
  frameCountChange() {
    if(!this.frameCount) {
      this.frameCountAlert = 'Enter valid number';
      return;
    } else if (this.frameCount > 7 || this.frameCount < 1) {
      this.frameCountAlert = 'Enter valid number between 1 and 7';
      return;
    } else {
      this.frameCountAlert = '';
    }

    const numbers: number[] = [];
    for(let i = 1; i <= this.frameCount; i++) {
      numbers.push(i);
    }

    this.frameList = numbers;
    this.variable.frames = this.frameList.join(',');

    if(this.frameCount > this.variable.frameCount) {
      for(let i = this.variable.frameCount + 1; i <= this.frameCount; i++) {
        this.variable.newFrame(i);
      }
    } else if(this.frameCount < this.variable.frameCount) {
      this.variable.trim(this.frameCount);
    }

    this.variable.frameCount = this.frameCount;
  }

  fileSeleced(event: Event, imageType: string, frame: number) {
    const file: File = ((event.target as HTMLInputElement).files as FileList).item(0) as File;
    if(!file) {
      return;
    }
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      (this.variable.frameVariables[frame - 1] as Record<string, any>)[imageType] = fileReader.result;
    }
  }

  export() {
    type exportType = {name: string, value: string}

    let animationValue = '{';
    const length = this.variable.frameVariables.length - 1;
    this.variable.frameVariables.forEach((frame, index) => {
      const animation = frame.animation;
      animationValue += '"F' + (index + 1) + '":{' + this.processAnimationType('bg', animation) + ','
       + this.processAnimationType('cta', animation) + ',' + this.processAnimationType('headline', animation) + ',' 
       + this.processAnimationType('image1', animation) + ',' + this.processAnimationType('image2', animation) + '}';
       
       if(length != index) {
        animationValue += ',';
       }
    })
    animationValue += '}';

    const exportObjects: exportType[] = [
      {name: 'Frame_Select', value: this.frameList.join(',')}, {name: 'Frame_Durations', value: this.variable.frameDelays.join(',')},
      {name: 'Animation_Direction', value: animationValue}, {name: 'BG_clickThrough_URL', value: this.variable.frameUrl},
      {name: 'CTA_clickthrough_URL', value: this.variable.ctaURL},
    ];

    this.variable.frameVariables.forEach((frame, index) => {
      let value: string;

      const prefix = 'F' + (index + 1);
      exportObjects.push({name: prefix + '_Background_Image', value: this.sanitize(frame.bgImage)})
      exportObjects.push({name: prefix + '_image1', value: this.sanitize(frame.image1)})
      exportObjects.push({name: prefix + '_image2', value: this.sanitize(frame.image2)});
      exportObjects.push({name: prefix + '_Copy', value: frame.headline? frame.headline: ''});

      const headlineControl = frame.headlineControl
      value = headlineControl.color + '|' + headlineControl.width + ',' + headlineControl.height + '|' + headlineControl.x + ',' 
        + headlineControl.y + '|' + headlineControl.hAlign.substring(0 , 1) + headlineControl.vAlign.substring(0, 1) + '|' + headlineControl.fontSize;
      exportObjects.push({name: prefix + '_Copy_Color_WH_XY_Align', value});

      exportObjects.push({name: prefix + '_CTA_Text', value: frame.cta? frame.cta: ''});

      const ctaControl = frame.ctaControl
      value = ctaControl.x + ',' + ctaControl.y + '|' + ctaControl.textColor + '|' + ctaControl.boxColor + '|' + ctaControl.rollColor
      exportObjects.push({name: 'F' + (index + 1) + '_CTA_xy_fgColor_bgColor_roColor', value});
    });


    this.http.post(this.link, JSON.stringify(exportObjects), {responseType: 'arraybuffer'}).subscribe((res: ArrayBuffer) => {
      const blob = new Blob([res], {type: 'application/zip'});
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.dispatchEvent(new MouseEvent('click'));

    })
  }

  processAnimationType (type: 'bg' | 'image1' | 'image2' | 'headline' | 'cta', animation: AnimationDirection): string {
    const animType = animation[type]
    return '"' + type + '": "' + animType.inDelay + ',' + animType.inType + ',' + animType.outDelay + ',' + animType.outType + '"'
  }

  sanitize(name: string): string {
    if(name.includes('null.png')) {
      name = name.replace('assets', '');
    }
    return name;
  }



}
