import { Component, Input, OnInit } from '@angular/core';
import { FrameVariables, Variables, transitionType, HeadlineControl, CtaControl } from '../variables';
import {gsap, Back, Bounce} from 'gsap';
import { Draggable } from 'gsap/Draggable'

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.css']
})
export class AdComponent implements OnInit {
  @Input() variables: Variables;
  @Input() changeTriggered: boolean;

  adWidth: number;
  adHeight: number;
  frames: number[] = [];
  adDimension: Record<string, string>;
  headlineTextControl: Record<string, string>[];
  firstTime = true;
  
  constructor() { }  ngOnInit(): void {    }

  ngAfterViewInit() {
    if(this.firstTime) {
      this.firstTime = false;
      this.setupFromVariables()
    }
  }

  ngOnChanges() {
    if(!this.changeTriggered) {
      return;
    }
    console.log(this.variables.fontLink)
    this.adWidth = this.variables.width;
    this.adHeight = this.variables.height;
    this.adDimension = {'width' : this.adWidth + 'px' , height: this.adHeight + 'px'};
    this.frames = [];
    for(let i = 1; i <= this.variables.frameCount; i++) {
      this.frames.push(i);
    }

    if(this.variables.frameUrl) {
      document.getElementById('main')?.addEventListener('click', () => window.open(this.variables.frameUrl, '_blank'));
    }

    this.headlineTextControl = this.variables.frameVariables.map((frame: FrameVariables) => {
      const color = frame.headlineControl.color;
      if(frame.headlineControl.vAlign == 'top') {
        return {'text-align': frame.headlineControl.hAlign, top: '0px', bottom: '', 'font-size': frame.headlineControl.fontSize + 'px', color}  
      } 
      else if(frame.headlineControl.vAlign == 'bottom') {
        return {'text-align': frame.headlineControl.hAlign, top: '', bottom: '0px', 'font-size': frame.headlineControl.fontSize + 'px', color}
      } else {
        return {'text-align': frame.headlineControl.hAlign, top: '', bottom: '', 'font-size': frame.headlineControl.fontSize + 'px', color}
      }
    });

    if(!this.firstTime) {
      setTimeout(() => this.setupFromVariables(), 250);
    }
    
  }

  setupFromVariables() {
    this.variables.frameVariables.forEach((frame: FrameVariables, index: number) => {
      const headlineControl = frame.headlineControl, animation = frame.animation, ctaControls = frame.ctaControl,
      frameNumber = index + 1, baseZ = frameNumber * 10;

      gsap.set('#copyArea' + frameNumber, {
        zIndex: baseZ + 3, width: headlineControl.width, height: headlineControl.height, top: headlineControl.y, left: headlineControl.x
      });
      gsap.set('#copyArea' + frameNumber, this.valueForAnimationType(frame.animation.headline.inType));

      this.setImageStyle('#bg' + frameNumber, baseZ, animation.bg.inType);
      this.setImageStyle('#image1_' + frameNumber, baseZ + 1, animation.image1.inType);
      this.setImageStyle('#image2_' + frameNumber, baseZ + 2, animation.image2.inType);

      gsap.set('#cta' + frameNumber, {
        color: ctaControls.textColor, backgroundColor: ctaControls.boxColor, left: ctaControls.x, top: ctaControls.y, zIndex: baseZ + 4
      })
      gsap.set('#cta' + frameNumber, this.valueForAnimationType(animation.cta.inType));
    })

    setTimeout(() => this.startAnimation(), 250);
  }

  valueForAnimationType(type: transitionType): gsap.TweenVars {
    let returnValue: gsap.TweenVars; 
    if(type == 'fade' || type == 'pop' || type == 'none') {
      returnValue = {opacity: 0, scale: 1};
    } else {
      
      switch(type) {
        case 'left': returnValue = {x: (-1 * this.adWidth) +'px'} ; break;
        case 'top': returnValue = {y: (-1 * this.adHeight) +'px'} ; break;
        case 'right': returnValue = {x: (this.adWidth) +'px'} ; break;
        case 'bottom': returnValue = {y: (this.adWidth) +'px'} ; break;
        case 'drop': returnValue = {opacity: 0, scale: 2} ; break;
        case 'grow': returnValue = {opacity: 0, scale: .5}; break;
      }
    }
    return returnValue
  }

  setImageStyle(imageId: string, zIndex: number, initialPosition: transitionType) {
    gsap.set(imageId, {zIndex, width: this.adWidth, height: this.adHeight})
    gsap.set(imageId, this.valueForAnimationType(initialPosition));

  }

  startAnimation(): void {
    const main = document.getElementById('main');
    gsap.registerPlugin(Draggable);

    // Draggable.create('.copyarea', {type: 'x,y', bounds: main, onDragEnd: (e: Event) => {
    //   let element = e.target as HTMLDivElement;
    //   if(!element.id.includes('copyArea') ) {
    //     element = element.parentElement as HTMLDivElement
    //   }
    //   const index = Number.parseInt(element.id.substring(element.id.length - 1));
    //   this.configureDrag(element, this.variables.frameVariables[index - 1].headlineControl);
    // }}); 

    // Draggable.create('.cta', {type: 'x,y', bounds: main, onDragEnd: (e: Event) => {
    //   const element = e.target as HTMLDivElement;
    //   const index = Number.parseInt(element.id.substring(element.id.length - 1));
    //   this.configureDrag(element, this.variables.frameVariables[index - 1].ctaControl);
    // }})

    const tl = gsap.timeline();

    tl.set('.frame', {display: 'none', 'opacity': 1});
    let currentLabel: string;
    for(let i = 1; i <= this.variables.frameCount; i++) {
      tl.set('#frame' + i, {display: 'block'});
    }

    for(let i = 1; i <= this.variables.frameCount; i++) {
      currentLabel = 'frame' + i + 'start';
      tl.addLabel(currentLabel);
      const currentAnimation = this.variables.frameVariables[i - 1].animation;

      this.createInAnimation(tl, currentAnimation.headline.inType, '#copyArea' + i, currentAnimation.headline.inDelay, currentLabel);
      this.createInAnimation(tl, currentAnimation.cta.inType, '#cta' + i, currentAnimation.cta.inDelay, currentLabel);
      this.createInAnimation(tl, currentAnimation.image1.inType, '#image1_' + i, currentAnimation.image1.inDelay, currentLabel);
      this.createInAnimation(tl, currentAnimation.image2.inType, '#image2_' + i, currentAnimation.image2.inDelay, currentLabel);
      this.createInAnimation(tl, currentAnimation.bg.inType, '#bg' + i, currentAnimation.bg.inDelay, currentLabel);

      currentLabel = 'inDone' + i;
      tl.addLabel(currentLabel, '+=' + this.variables.frameDelays[(i - 1)]);

      this.createOutAnimation(tl, currentAnimation.headline.outType, '#copyArea' + i, currentAnimation.headline.outDelay, currentLabel);
      this.createOutAnimation(tl, currentAnimation.cta.outType, '#cta' + i, currentAnimation.cta.outDelay, currentLabel);
      this.createOutAnimation(tl, currentAnimation.image1.outType, '#image1_' + i, currentAnimation.image1.outDelay, currentLabel);
      this.createOutAnimation(tl, currentAnimation.image2.outType, '#image2_' + i, currentAnimation.image2.outDelay, currentLabel);
      this.createOutAnimation(tl, currentAnimation.bg.outType, '#bg' + i, currentAnimation.bg.outDelay, currentLabel);
    }
  }

  configureDrag(element: HTMLDivElement, elementControl: HeadlineControl | CtaControl) {
      let transform = element.style.transform;
      const bracketStart = transform.indexOf('(');
      const bracketEnd = transform.indexOf(')');
      const transfromList = transform.substring(bracketStart + 1, bracketEnd).split(',');

      elementControl.y += Number.parseInt(transfromList[1]);
      elementControl.x += Number.parseInt(transfromList[0]);

      gsap.set(element, {x: 0, left: elementControl.x, y: 0, top: elementControl.y});
  }

  createInAnimation(tl: gsap.core.Timeline, inType: transitionType, div: string, delay: number, label: string) {
    if(inType == 'none') {
      return;
    }
    if(inType == 'grow') {
      tl.to(div, {opacity: 1, ease: Back.easeOut.config(4), scale: 1, delay, duration: .5}, label)
    } else if(inType == 'drop') {
      tl.to(div, {opacity: 1, ease: Bounce.easeOut, scale: 1, delay, duration: .5}, label);
    } else if(inType != 'pop') {
      
      tl.to(div, {opacity: 1, x: 0, y: 0, delay, duration: .5}, label);
    } else {
      tl.set(div, {opacity: 1, delay}, label)
    }
  }

  createOutAnimation(tl: gsap.core.Timeline, outType: transitionType, div: string, delay: number, label: string) {
    if(outType == 'none') {
      return;
    }

    const duration = .5;
    const outObject: Record<string, gsap.TweenVars> = {
      pop: {opacity: 0, delay}, fade: {opacity: 0, delay, duration}, 
      left: {x: -1 * this.adWidth, delay, duration}, right: {x: this.adWidth, delay, duration}, 
      top: {y: -1 * this.adHeight, delay, duration}, bottom: {y: this.adHeight, delay, duration},
      grow: {scale: .5, opacity: 0, delay, duration}, drop: {scale: 2, opacity: 0, delay, duration}
    }

    if(outType != 'pop') {
      tl.to(div, outObject[outType], label)
    } else {
      tl.set(div, outObject[outType], label)
    }
  }
}

