import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appNumberOnly]'
})
export class NumberOnlyDirective {
  navigationKeys = ['Backspace', 'Tab', 'Delete', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'];
  metakeys = ['a', 'c', 'v', 'x'];

  constructor() {}

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    const key = event.key;
    if(this.navigationKeys.includes(key)) {
      return;
    }
    if((event.metaKey || event.ctrlKey) && this.metakeys.includes(key)) {
      return;
    }

    const number = Number.parseInt(key);
    if(number == 0) {
      return
    }
    if(!number) {
      event.preventDefault();
    }
  }

}
