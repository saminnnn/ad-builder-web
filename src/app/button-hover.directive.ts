import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appButtonHover]'
})
export class ButtonHoverDirective {
  element: HTMLDivElement;
  @Input() appButtonHover = '';
  @Input() defaultCTABGColor = '';
  
  constructor(private el: ElementRef) {
    this.element = this.el.nativeElement;
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.element.style.backgroundColor = this.appButtonHover;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.element.style.backgroundColor = this.defaultCTABGColor;
  }

}
