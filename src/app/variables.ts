export type transitionType = 'left'|'right'|'top'|'bottom'|'fade'|'pop'|'drop'|'grow'|'none';

export interface FrameVariables {
    headline: string|null, headlineControl: HeadlineControl, cta: string| null, ctaControl: CtaControl, 
    bgImage: string, image1: string, image2: string, animation: AnimationDirection
}

export interface HeadlineControl {
    width: number, height: number, hAlign: 'left'|'center'|'right', vAlign: 'top'|'center'|'bottom', x: number, y: number, color: string, fontSize: number
}

export interface AnimationDirection { headline: AnimationType, cta: AnimationType, bg: AnimationType, image1: AnimationType, image2: AnimationType}

export interface CtaControl {textColor: string, boxColor: string, rollColor: string, x: number, y: number}

export class Variables {
    frames: string;
    frameCount: number = 1;
    frameDelays: number[] = [];
    ctaURL: string = '';
    frameUrl: string = '';
    name: string; 
    frameVariables: FrameVariables[] = [];
    nullImage = 'assets/null.png';
    height: number = 250;
    width: number = 300;
    fontLink: string;
    fontName: string;
    fontStyle: string = 'normal'

    private standardHeadline: HeadlineControl = {
        width: 300, height: 100, hAlign: 'center', vAlign: 'top', x: 0, y: 100, color: '#333333', fontSize: 18
    };
    private standardCtaControl: CtaControl = {
        textColor: "#ffffff", boxColor: "#003057", rollColor: "#009057", x: 179, y: 206
    };

    private standardAnimationType: AnimationType = {inType: 'fade', inDelay: 0, outType: 'none', outDelay: 0};

    constructor() {
        this.name = 'New Ad'
        this.frames = '1';
        this.newFrame(1);
    }

    newFrame(index: number) {
        this.frameVariables.push({
            headline: 'Frame' + index + 'Headline', headlineControl: {...this.standardHeadline}, cta: 'learn more', ctaControl: {...this.standardCtaControl},
            bgImage: this.nullImage, image1: this.nullImage, image2: this.nullImage, animation: {
                headline: {...this.standardAnimationType}, cta: {...this.standardAnimationType}, 
                bg: {...this.standardAnimationType}, image1: {...this.standardAnimationType}, image2: {...this.standardAnimationType}, 
            }
        })
        this.frameDelays.push(2); 
    }

    trim(count: number) {
        this.frameVariables = this.frameVariables.slice(0, count);
        this.frameDelays = this.frameDelays.slice(0, count);
    }
}

interface AnimationType {  inType: transitionType, inDelay: number, outType: transitionType, outDelay: number}



