import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';
import { ConfigureAdComponent } from './configure-ad/configure-ad.component';
import { FormsModule } from '@angular/forms';
import { NumberOnlyDirective } from './number-only.directive';
import { ButtonHoverDirective } from './button-hover.directive';

@NgModule({
  declarations: [
    AppComponent,
    AdComponent,
    ConfigureAdComponent,
    NumberOnlyDirective,
    ButtonHoverDirective
  ],
  imports: [
    BrowserModule, AppRoutingModule, FormsModule, HttpClientModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
