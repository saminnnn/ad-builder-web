var values = [   
    {
        "name": "Frame_Select",
        "value": "1,2,3,4"
    },
    {
        "name": "Frame_Durations",
        "value": ""
    },
    {
        "name": "Animation_Direction",
        "value":`{
            "F1":{"Background_Image":"0,fade,0,none",
           "Logo":".5,fade,0,none",
           "Overlay_Image":".5,top,0,right",
           "Copy":"1,left,.5,right",
           "CTA_Text":"1,fade,0,none"},
           
           "F2":{"Background_Image":"0,top,0,none",
           "Logo":"0,none, 0,none",
           "Overlay_Image":".5,left,0,right",
           "Copy":"1,left,.5,right",
           "CTA_Text":"0,none,0,none"},
           
           "F3":{"Background_Image":"0,none,0,none",
           "Logo":"0,none, 0,none",
           "Overlay_Image":".5,left,0,right",
           "Copy":"1,left,.5,right",
           "CTA_Text":"0,none,0,none"},
           
           "F4":{"Background_Image":"0,none,0,none",
           "Logo":"0,none, 0,none",
           "Overlay_Image":"0,fade,0,none",
           "Copy":"0,fade,0,none",
           "CTA_Text":"0,grow,0,none"}
           }`
    },
    {
        "name": "insertCSS",
        "value": ""
    },
    {
        "name": "CTA_clickthrough_URL",
        "value": ""
    },
    {
        "name": "BG_clickThrough_URL",
        "value": ""
    },
    {
        "name": "F1_Background_Image",
        
        "value": "images/f1_bg_300x250.jpg"
    },
    {
        "name": "F1_Logo",
        "value": "images/f1_logo_300x250.png"
    },
    {
        "name": "F1_Overlay_Image",
        
        "value": "images/heartbeat_300x250.png"
    },
    {
        "name": "F1_Copy",
        "value": "A more human way to healthcare"
    },
    {
        "name": "F1_Copy_Color_WH_XY_Align",
        "value": "#003057|300,200|0,145|ct"
    },
    {
        "name": "F1_CTA_Text",
        "value": "learn more"
    },
    {
        "name": "F1_CTA_xy_fgColor_bgColor_roColor",
        "value": "179,206|#ffffff|#003057|#003057"
    },
    {
        "name": "F2_Background_Image",
        "value": "images/f2_bg_300x250.jpg"
    },
    {
        "name": "F2_Logo",
        
        "value": "images/null.png"
    },
    {
        "name": "F2_Overlay_Image",
        
        "value": "images/bag_300x250.png"
    },
    {
        "name": "F2_Copy",
        "value": "We don't just look<br>out for your health."
    },
    {
        "name": "F2_Copy_Color_WH_XY_Align",
        "value": "#003057|300,200|0,120|ct"
    },
    {
        "name": "F2_CTA_Text",
        "value": ""
    },
    {
        "name": "F2_CTA_xy_fgColor_bgColor_roColor",
        "value": ""
    },
    {
        "name": "F3_Background_Image",
        "value": "images/f3_bg_300x250.jpg"
    },
    {
        "name": "F3_Logo",
        "value": "images/null.png"
    },
    {
        "name": "F3_Overlay_Image",
        
        "value": "images/card_300x250.png"
    },
     {
        "name": "F3_Copy",
        
        "value": "We look out<br> for your<br>bottom line too."
    },
    {
        "name": "F3_Copy_Color_WH_XY_Align",
        
        "value": "#003057|300,200|0,120|ct"
    },      
    {
        "name": "F3_CTA_Text",
        
        "value": ""
    },
    {
        "name": "F3_CTA_xy_fgColor_bgColor_roColor",
        
        "value": ""
    },
    {
        "name": "F4_Background_Image",
        
        "value": "images/f2_bg_300x250.jpg"
    },
    {
        "name": "F4_Logo",
        
        "value": "images/null.png"
    },
    {
        "name": "F4_Overlay_Image",
        
        "value": "images/things_300x250.png"
    },
    {
        "name": "F4_Copy",
        
        "value": "A more<br>human way to<br><span style='line-height: .8em'>healthcare<sup>TM<sup></span>"
    },
    {
        "name": "F4_Copy_Color_WH_XY_Align",
        
        "value": "#003057|300,200|0,62|ct"
    },
    {
        "name": "F4_CTA_Text",
        
        "value": "learn more"
    },
    {
        "name": "F4_CTA_xy_fgColor_bgColor_roColor",
        
        "value": "179,206|#ffffff|#003057|#003057"
    },
    {
        "name": "F5_Background_Image",
        
        "value": "images/null.png"
    },
    {
        "name": "F5_Logo",
        
        "value": "images/null.png"
    },
    {
        "name": "F5_Overlay_Image",
        
        "value": "images/null.png"
    },
    {
        "name": "F5_Copy",
        
        "value": "Explore Medicare<br>Advantage plans with<br>premium starting at<br><b>$0/month.</b>"
    },
    {
        "name": "F5_Copy_Color_WH_XY_Align",
        
        "value": "#003057|300,200|0,38|ct"
    },
    {
        "name": "F5_CTA_Text",
        
        "value": "VIEW PLANS"
    },
    {
        "name": "F5_CTA_xy_fgColor_bgColor_roColor",
        
        "value": "161,197|#ffffff|#af0061|#612166"
    },
    {
        "name": "F6_Background_Image",
        
        "value": "images/null.png"
    },
    {
        "name": "F6_Logo",
        
        "value": "images/null.png"
    },
    {
        "name": "F6_Overlay_Image",
        
        "value": "images/null.png"
    },
    {
        "name": "F6_Copy",
        
        "value": ""
    },
    {
        "name": "F6_Copy_Color_WH_XY_Align",
        
        "value": ""
    },
    {
        "name": "F6_CTA_Text",
        
        "value": ""
    },
    {
        "name": "F6_CTA_xy_fgColor_bgColor_roColor",
        
        "value": ""
    },
    {
        "name": "F7_Background_Image",
        
        "value": "images/null.png"
    },
    {
        "name": "F7_Logo",
        
        "value": "images/null.png"
    },
    {
        "name": "F7_Overlay_Image",
        
        "value": "images/null.png"
    },
    {
        "name": "F7_Copy",
        
        "value": "images/null.png"
    },
    {
        "name": "F7_Copy_Color_WH_XY_Align",
        
        "value": ""
    },
    {
        "name": "F7_CTA_Text",
        
        "value": ""
    },
    {
        "name": "F7_CTA_xy_fgColor_bgColor_roColor",
        
        "value": ""
    }
];

var variables = {};
var length = values.length;

for(i = 0; i < length; i++) {
    var key = values[i].name;
    var value = values[i].value;

    variables[key]  = value;
}



