const fs = require('fs/promises');
const path = require('path');
const appRoot = require('app-root-path').toString();
const intervelTime = 1 * 60 * 1000;
let i = 1

setInterval(() => {
    console.log('Sweep ' + i);
    i++;

    fs.readdir(path.join(appRoot, 'zipFiles')).then(dirNames => {
        dirNames.forEach(dirName => {
            const pathToDir = getPathToDirectory(dirName);
            fs.stat(pathToDir).then(stat => {
                const birthTime = new Date(stat.birthtime).getTime();
                const currentTime = new Date().getTime();
        
                if(currentTime - birthTime > intervelTime) {
                    fs.rmdir(pathToDir, {recursive: true}).catch(e => {
                        if(e) console.log(e);
                    }).catch(e => {
                        console.log('Error for delete');
                        console.log(e);
                    })
                }
            }).catch(e => {
                console.log('Error for stat');
                console.log(e);
            })
        });
    });
}, intervelTime)






// fs.readdir().then(dirNames => {
//     dirNames.forEach(dirName)
// }).catch(e => {
//     console.log(e);
// })

/*fs.stat(directoryPath).then(stats => {
    const birthTime = new Date(stats.birthtime).getTime();
    const currentTime = new Date().getTime();

    if(currentTime - birthTime > (10 * 60 * 1000)) {
        fs.rmdir(directoryPath, {recursive: true}).catch(e => {
            if(e) console.log(e);
        })
    }
})*/

function getPathToDirectory(dirName) {
    return path.join(appRoot, 'zipFiles', dirName);
}

