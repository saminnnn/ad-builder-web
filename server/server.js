const express = require('express');
var fs = require('fs');
var fsPromise = require('fs/promises');
var path = require('path');
const app = express();
const appRoot = require('app-root-path').toString();
const randToken = require('rand-token');
const childProcess = require('child_process');

const configText = fs.readFileSync(path.join(appRoot, 'server', 'config.json'), {encoding: 'UTF8'});
const config = JSON.parse(configText);
const port = config.port;
const indexHtml = fs.readFileSync(path.join(appRoot, config.staticFileLocation, 'index.html'));
const nullImage = fs.readFileSync(path.join(appRoot, config.staticFileLocation, 'null.png'));
const css = fs.readFileSync(path.join(appRoot, config.staticFileLocation, 'style.css'));
const js = fs.readFileSync(path.join(appRoot, config.staticFileLocation, 'script.js'));

app.use(express.text({limit: 300 * 1024}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Connection');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    if (req.method == 'OPTIONS') {
      res.sendStatus(200);
    }
    else {
      next();
    }
});

app.get('/', (req, res) => res.send('hello World'));

app.post('/variables', (req, res) => {
    const body = 'var values =' + req.body + `
    var variables = {};
    var length = values.length;

    for(i = 0; i < length; i++) {
        var key = values[i].name;
        var value = values[i].value;

        variables[key]  = value;
    }`;
    const folderName = randToken.generate(5);
    const zipFileName = 'name.zip';
    const currentProjectPath = path.join(appRoot ,config.destination, folderName);
    const cssPath = path.join(currentProjectPath, 'css');
    const jsPath = path.join(currentProjectPath, 'js');
    

    fsPromise.mkdir(currentProjectPath).then(() => {
        return fsPromise.mkdir(cssPath);
    }).then(() => {
        return fsPromise.mkdir(jsPath);
    }).then(() => {
        return fsPromise.writeFile(path.join(currentProjectPath, 'index.html'), indexHtml);
    }).then(() => {
        return fsPromise.writeFile(path.join(currentProjectPath, 'null.png'), nullImage);
    }).then(() => {
        return fsPromise.writeFile(path.join(jsPath, 'script.js'), js);
    }).then(() => {
        return fsPromise.writeFile(path.join(jsPath, 'variables.js'), body);
    }).then(() => {
        return fsPromise.writeFile(path.join(cssPath, 'style.css'), css);
    }).then(() => {
        const outputPath = path.join(currentProjectPath, zipFileName);
        // const folderPath = path.join(currentProjectPath, '*');
        const command = 'tar -a -c -f "' + outputPath + '" -C "' + currentProjectPath + '" .';
        console.log(command);
        childProcess.exec(command, (err, out, stderr) => {
            if(!err) {
                res.sendFile(outputPath);
            }
        });
    }).catch(e => {
        res.send(e);
    });
});

const message = 'serving zip';
app.listen(port, () => {console.log('Listening on port ' + port + '\n' + message)});